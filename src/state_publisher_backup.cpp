#include <string>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <tf/transform_broadcaster.h>



int main(int argc, char** argv) {
    ros::init(argc, argv, "intial_state_publisher");
    ros::NodeHandle n;
    ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);
    tf::TransformBroadcaster broadcaster;
    ros::Rate loop_rate(30);

 //   const double degree = M_PI/180;

    // robot state
    double angle=0, tinc=.03, waist_tilt_joint=0, waist_rotate_joint=0,
    waist_bow_joint=0,right_thigh_bend_joint=0,left_thigh_bend_joint=0,
    right_thigh_lat_rotate_joint=0,left_thigh_lat_rotate_joint=0,
    right_thigh_abduction_joint=0,left_thigh_abduction_joint=0,
    right_knee_flex_joint=0,left_knee_flex_joint=0,
    right_foot_inversion_joint=0,left_foot_inversion_joint=0,
    right_foot_lat_rotation_joint=0,left_foot_lat_rotation_joint=0,
    right_foot_flex_joint=0,left_foot_flex_joint=0,right_shoulder_circ_joint=0,
    left_shoulder_circ_joint=0,right_shoulder_abduction_joint=0,
    left_shoulder_abduction_joint=0,right_elbow_flex_joint=0,
    left_elbow_flex_joint=0,right_wrist_pronation_joint=0,left_wrist_pronation_joint=0,
    right_wrist_flex_joint=0,left_wrist_flex_joint=0,neck_joint_3=0,
    neck_tilt_joint_2=0,neck_nod_joint_2=0,neck_tilt_joint_1=0,
    head_tilt_joint=0,neck_nod_joint_1=0,head_lateral_ext_joint=0,
    head_shake_joint=0,head_nod_joint=0;
    
   
	//*** initialize here
	
    // message declarations
    geometry_msgs::TransformStamped odom_trans;
    sensor_msgs::JointState joint_state;
    odom_trans.header.frame_id = "odom";
    odom_trans.child_frame_id = "upper_torso"; 

    while (ros::ok()) {
        //update joint_state
        joint_state.header.stamp = ros::Time::now();
	    joint_state.header.frame_id="upper_torso";
        joint_state.name.resize(36);
        joint_state.position.resize(36);
        
        joint_state.name[0] = "waist_tilt_joint";
        joint_state.position[0] = waist_tilt_joint;    
        joint_state.name[1] = "waist_rotate_joint";
        joint_state.position[1] =waist_rotate_joint;    
        joint_state.name[2] ="waist_bow_joint";
        joint_state.position[2] =waist_bow_joint;              
        joint_state.name[3] ="right_thigh_bend_joint";
        joint_state.position[3] =right_thigh_bend_joint;    
        joint_state.name[4] ="left_thigh_bend_joint";
        joint_state.position[4] =left_thigh_bend_joint;   
        joint_state.name[5] ="right_thigh_lat_rotate_joint";
        joint_state.position[5] =right_thigh_lat_rotate_joint;    
        joint_state.name[6] ="left_thigh_lat_rotate_joint";
        joint_state.position[6] =left_thigh_lat_rotate_joint;   
        joint_state.name[7] ="right_thigh_abduction_joint";
        joint_state.position[7] =right_thigh_abduction_joint;    
        joint_state.name[8] ="left_thigh_abduction_joint";
        joint_state.position[8] =left_thigh_abduction_joint;   
        joint_state.name[9] ="right_knee_flex_joint";
        joint_state.position[9] =right_knee_flex_joint;    
        joint_state.name[10] ="left_knee_flex_joint";
        joint_state.position[10] =left_knee_flex_joint;              
        joint_state.name[11] ="right_foot_inversion_joint";
        joint_state.position[11] =right_foot_inversion_joint;    
        joint_state.name[12] ="left_foot_inversion_joint";
        joint_state.position[12] =left_foot_inversion_joint;   
        joint_state.name[13] ="right_foot_lat_rotation_joint";
        joint_state.position[13] =right_foot_lat_rotation_joint;    
        joint_state.name[14] ="left_foot_lat_rotation_joint";
        joint_state.position[14] =left_foot_lat_rotation_joint;   
        joint_state.name[15] ="right_foot_flex_joint";
        joint_state.position[15] =right_foot_flex_joint;    
        joint_state.name[16] ="left_foot_flex_joint";
        joint_state.position[16] =left_foot_flex_joint;
        joint_state.name[17] ="right_shoulder_circ_joint";
        joint_state.position[17] =right_shoulder_circ_joint;    
        joint_state.name[18] ="left_shoulder_circ_joint";
        joint_state.position[18] =left_shoulder_circ_joint;
        joint_state.name[19] ="right_shoulder_abduction_joint";
        joint_state.position[19] =right_shoulder_abduction_joint;
        joint_state.name[20] ="left_shoulder_abduction_joint";
        joint_state.position[20] =left_shoulder_abduction_joint;    
        joint_state.name[21] ="right_elbow_flex_joint";
        joint_state.position[21] =right_elbow_flex_joint;        
        joint_state.name[22] ="left_elbow_flex_joint";
        joint_state.position[22] =left_elbow_flex_joint;
        joint_state.name[23] ="right_wrist_pronation_joint";
        joint_state.position[23] =right_wrist_pronation_joint;    
        joint_state.name[24] ="left_wrist_pronation_joint";
        joint_state.position[24] =left_wrist_pronation_joint;
        joint_state.name[25] ="right_wrist_flex_joint";
        joint_state.position[25] =right_wrist_flex_joint;
        joint_state.name[26] ="left_wrist_flex_joint";
        joint_state.position[26] =left_wrist_flex_joint;    
        joint_state.name[27] ="neck_joint_3";
        joint_state.position[27] =neck_joint_3;        
		joint_state.name[28] ="neck_tilt_joint_2";
        joint_state.position[28] =neck_tilt_joint_2;
        joint_state.name[29] ="neck_nod_joint_2";
        joint_state.position[29] =neck_nod_joint_2;    
        joint_state.name[30] ="neck_tilt_joint_1";
        joint_state.position[30] =neck_tilt_joint_1;
        joint_state.name[31] ="neck_nod_joint_1";
        joint_state.position[31] =neck_nod_joint_1;
        joint_state.name[32] ="head_tilt_joint";
        joint_state.position[32] =head_tilt_joint;    
        joint_state.name[33] ="head_lateral_ext_joint";
        joint_state.position[33] =head_lateral_ext_joint;        
        joint_state.name[34] ="head_shake_joint";
        joint_state.position[34] =head_shake_joint;
        joint_state.name[35] ="head_nod_joint";
        joint_state.position[35] =head_nod_joint;    
                                        
        //send the joint state and transform
        joint_pub.publish(joint_state);
        broadcaster.sendTransform(odom_trans);
        
        //create robot state      
/*        
        waist_tilt_joint += tinc;
        if (waist_tilt_joint<-1.57 || waist_tilt_joint>1.57) tinc *= -1;

        waist_rotate_joint += tinc;
        if (waist_rotate_joint<-1.57 || waist_rotate_joint>1.57) tinc *= -1;

        waist_bow_joint += tinc;
        if (waist_bow_joint<-1.57 || waist_bow_joint>1.57) tinc *= -1;
        
        right_thigh_bend_joint += tinc;
        if (right_thigh_bend_joint<-1.57 || right_thigh_bend_joint>1.57) tinc *= -1;
        
        left_thigh_bend_joint += tinc;
        if (left_thigh_bend_joint<-1.57 || left_thigh_bend_joint>1.57) tinc *= -1;

        right_thigh_lat_rotate_joint += tinc;
        if (right_thigh_lat_rotate_joint<-1.57 || right_thigh_lat_rotate_joint>1.57) tinc *= -1;
        
        left_thigh_lat_rotate_joint += tinc;
        if (left_thigh_lat_rotate_joint<-1.57 || left_thigh_lat_rotate_joint>1.57) tinc *= -1;
        
        right_thigh_abduction_joint += tinc;
        if (right_thigh_abduction_joint<-1.57 || right_thigh_abduction_joint>1.57) tinc *= -1;

        left_thigh_abduction_joint += tinc;
        if (left_thigh_abduction_joint<-1.57 || left_thigh_abduction_joint>1.57) tinc *= -1;

        right_knee_flex_joint += tinc;
        if (right_knee_flex_joint<-1.57 || right_knee_flex_joint>1.57) tinc *= -1;
        
        left_knee_flex_joint += tinc;
        if (left_knee_flex_joint<-1.57 || left_knee_flex_joint>1.57) tinc *= -1;
        
        right_foot_inversion_joint += tinc;
        if (right_foot_inversion_joint<-1.57 || right_foot_inversion_joint>1.57) tinc *= -1;

        left_foot_inversion_joint += tinc;
        if (left_foot_inversion_joint<-1.57 || left_foot_inversion_joint>1.57) tinc *= -1;
        
        right_foot_lat_rotation_joint += tinc;
        if (right_foot_lat_rotation_joint<-1.57 || right_foot_lat_rotation_joint>1.57) tinc *= -1;
        
		left_foot_lat_rotation_joint += tinc;
        if (left_foot_lat_rotation_joint<-1.57 || left_foot_lat_rotation_joint>1.57) tinc *= -1;
        
        right_foot_flex_joint += tinc;
        if (right_foot_flex_joint<-1.57 || right_foot_flex_joint>1.57) tinc *= -1;

        left_foot_flex_joint += tinc;
        if (left_foot_flex_joint<-1.57 || left_foot_flex_joint>1.57) tinc *= -1;

        right_shoulder_circ_joint += tinc;
        if (right_shoulder_circ_joint<-1.57 || right_shoulder_circ_joint>1.57) tinc *= -1;
        
        left_shoulder_circ_joint += tinc;
        if (left_shoulder_circ_joint<-1.57 || left_shoulder_circ_joint>1.57) tinc *= -1;
        
        right_shoulder_abduction_joint += tinc;
        if (right_shoulder_abduction_joint<-1.57 || right_shoulder_abduction_joint>1.57) tinc *= -1;

        left_shoulder_abduction_joint += tinc;
        if (left_shoulder_abduction_joint<-1.57 || left_shoulder_abduction_joint>1.57) tinc *= -1;
        
        right_elbow_flex_joint += tinc;
        if (right_elbow_flex_joint<-1.57 || right_elbow_flex_joint>1.57) tinc *= -1;
        
        left_elbow_flex_joint += tinc;
        if (left_elbow_flex_joint<-1.57 || left_elbow_flex_joint>1.57) tinc *= -1;

        right_wrist_pronation_joint += tinc;
        if (right_wrist_pronation_joint<-1.57 || right_wrist_pronation_joint>1.57) tinc *= -1;

        left_wrist_pronation_joint += tinc;
        if (left_wrist_pronation_joint<-1.57 || left_wrist_pronation_joint>1.57) tinc *= -1;
        
        right_wrist_flex_joint += tinc;
        if (right_wrist_flex_joint<-1.57 || right_wrist_flex_joint>1.57) tinc *= -1;
        
        left_wrist_flex_joint += tinc;
        if (left_wrist_flex_joint<-1.57 || left_wrist_flex_joint>1.57) tinc *= -1;

        neck_tilt_joint_2 += tinc;
        if (neck_tilt_joint_2<-1.57 || neck_tilt_joint_2>1.57) tinc *= -1;
        
		neck_nod_joint_2 += tinc;
        if (neck_nod_joint_2<-1.57 || neck_nod_joint_2>1.57) tinc *= -1;
        
        neck_tilt_joint_1 += tinc;
        if (neck_tilt_joint_1<-1.57 || neck_tilt_joint_1>1.57) tinc *= -1;

        neck_nod_joint_1 += tinc;
        if (neck_nod_joint_1<-1.57 || neck_nod_joint_1>1.57) tinc *= -1;

        head_tilt_joint += tinc;
        if (head_tilt_joint<-1.57 || head_tilt_joint>1.57) tinc *= -1;
        
        head_lateral_ext_joint += tinc;
        if (head_lateral_ext_joint<-1.57 || head_lateral_ext_joint>1.57) tinc *= -1;
        
        head_shake_joint += tinc;
        if (head_shake_joint<-1.57 || head_shake_joint>1.57) tinc *= -1;

        head_nod_joint += tinc;
        if (head_nod_joint<-1.57 || head_nod_joint>1.57) tinc *= -1;
        */
        
		ros::spinOnce();
        loop_rate.sleep();
    }


    return 0;
}
